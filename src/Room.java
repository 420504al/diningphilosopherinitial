
public class Room {
  private int compte;
  private int enAttente;
  public final int MAX = 4;

  Room() {
    compte = 0;
    enAttente = 0;
  }

  synchronized void entrer() {
    while(compte == MAX || enAttente > 0) {
      try {
        enAttente++;
        wait();
        enAttente--;
      } catch(InterruptedException e) {
        System.out.println(e);
      }
    }
    compte++;
  }

  synchronized void sortir() {
    compte--;
    notify();
  }

}
